# Pixlys Desktop Client
This repository contains desktop clients for Windows, macOS and Linux based
operating systems.

https://pixlys.com

## Building from source (Linux)
Install virtualenv and pyinstaller:
`pip3 install virtualenv pyinstaller`

Create and activate virtualenv:
`python3 -m virtualenv --no-site-packages env && source env/bin/activate`

Install dependencies:
`pip3 install -r requirements.txt`

Start bundling script:
`./distribution/debian-build.sh`

Application is built into `dist/pixlys-<version>`. Start with:
`./dist/pixlys-<version>/pixlys`

## Building from source (Windows)
Note that the following steps depends on GIT Bash for Windows being used.

Install virtualenv and pyinstaller:
`py.exe -m pip install virtualenv pyinstaller`

Create and activade virtualenv:
`py.exe -m virtualenv --no-site-packages env && source env/Scripts/activate`

Install dependencies:
`py.exe -m pip install -r requirements.txt`

Start bundling script:
`./distribution/windows-build.sh`

Application is built into `dist/pixlys-<version>`. Start with:
`./dist/pixlys-<version>/pixlys`

## Building from source (macOS)
Same as Linux, but use darwin bundling script instead.

### Package builds into installers
TBD

## Licence
Pixlys Desktop Client is released under GPLv3 licence. See the LICENSE file for
details.
