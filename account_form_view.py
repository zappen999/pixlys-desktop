from PyQt5 import QtCore
from PyQt5.QtWidgets import (QWidget, QPushButton, QHBoxLayout, QVBoxLayout,
                             QLabel, QLineEdit, QCheckBox)

from config import APP_NAME, PRIVACY_URL, TOS_URL
from style import get_link


class AccountFormView(QWidget):
    def __init__(self, login=True):
        super().__init__()

        self.layout = None
        self.login = login

        self.email_field = None
        self.password_field = None
        self.back_btn = None
        self.submit_btn = None

        self.init_ui()

    def on_submit(self, callback):
        self.submit_btn.clicked.connect(
                lambda: callback(self.email_field.text(),
                                 self.password_field.text()))

    def on_back(self, callback):
        self.back_btn.clicked.connect(callback)

    def init_ui(self):
        self.layout = QVBoxLayout(self)
        btn_group = QHBoxLayout()

        self.layout.addWidget(QLabel('Your email address'))
        self.email_field = QLineEdit()
        self.layout.addWidget(self.email_field)

        self.layout.addWidget(QLabel('Your password' + (
            ' (min 8 characters)' if not self.login else '')))
        self.password_field = QLineEdit()
        self.password_field.setEchoMode(QLineEdit.Password)
        self.layout.addWidget(self.password_field)

        self.back_btn = QPushButton('← Back')
        QWidget.setTabOrder(self.password_field, self.submit_btn)
        btn_group.addWidget(self.back_btn, 0, QtCore.Qt.AlignLeft)

        self.submit_btn = QPushButton(
                'Sign in' if self.login else 'Create account')
        self.submit_btn.setDefault(True)
        btn_group.addWidget(self.submit_btn, 0, QtCore.Qt.AlignRight)

        self.layout.addStretch()

        if not self.login:
            # We need the user to accept the policies first
            self.submit_btn.setEnabled(False)
            policy_group = QHBoxLayout()

            policy_chk = QCheckBox()
            policy_chk.stateChanged.connect(
                    lambda: self.submit_btn.setEnabled(policy_chk.isChecked()))
            policy_group.addWidget(policy_chk)

            policy_lbl = QLabel('I accept %s %s and<br>%s' % (
                    APP_NAME,
                    get_link(PRIVACY_URL, 'Privacy policy'),
                    get_link(TOS_URL, 'Terms of Service')))
            policy_lbl.setOpenExternalLinks(True)
            policy_group.addWidget(policy_lbl)
            policy_group.addStretch()

            self.layout.addLayout(policy_group)

        self.layout.addLayout(btn_group)
