from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QMessageBox

from guest_account_view import GuestAccountView
from account_form_view import AccountFormView
from member_view import MemberView

from exception import (InvalidCredentialsException, AccountExistsException,
                       ValidationException)
from config import RESET_PASSWORD_URL
from style import get_link
import api
import state
import event


class AccountTab(QWidget):
    def __init__(self):
        super().__init__()

        self.layout = None

        self.guest_view = None
        self.login_form_view = None
        self.signup_form_view = None
        self.member_view = None

        self.init_ui()

    def init_ui(self):
        self.layout = QVBoxLayout(self)
        self.guest_view = GuestAccountView()
        self.login_form_view = AccountFormView(login=True)
        self.signup_form_view = AccountFormView(login=False)
        self.member_view = MemberView()

        # Connect event handlers
        self.guest_view.on_login(self.show_login_view)
        self.login_form_view.on_back(self.show_guest_view)
        self.login_form_view.on_submit(self.handle_login_click)

        self.guest_view.on_signup(self.show_signup_view)
        self.signup_form_view.on_back(self.show_guest_view)
        self.signup_form_view.on_submit(self.handle_signup_click)

        self.member_view.on_logout(self.handle_logout)

        self.layout.addWidget(self.guest_view)
        self.layout.addWidget(self.login_form_view)
        self.layout.addWidget(self.signup_form_view)
        self.layout.addWidget(self.member_view)

        event.on([event.EVENT['USER:LOGIN'],
                  event.EVENT['USER:SIGNUP']], self.handle_auth_success)

        # Start out as hidden
        self.guest_view.hide()
        self.login_form_view.hide()
        self.signup_form_view.hide()
        self.member_view.hide()

        # Determine what to show depending on user state
        user_email = state.read('user/email')
        self.show_member_view() if user_email else self.show_guest_view()

    def show_guest_view(self):
        self.login_form_view.hide()
        self.signup_form_view.hide()
        self.member_view.hide()
        self.guest_view.show()

    def show_login_view(self):
        self.guest_view.hide()
        self.signup_form_view.hide()
        self.member_view.hide()
        self.login_form_view.show()

    def show_signup_view(self):
        self.guest_view.hide()
        self.login_form_view.hide()
        self.member_view.hide()
        self.signup_form_view.show()

    def show_member_view(self):
        self.guest_view.hide()
        self.login_form_view.hide()
        self.signup_form_view.hide()
        self.member_view.show()

    def handle_auth_success(self, user):
        state.write('user/email', user['email'])
        event.emit(event.EVENT['USER:UPDATED'], user)
        self.show_member_view()

    def handle_logout(self):
        state.reset()
        self.show_guest_view()

    def handle_login_click(self, email, password):
        QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)

        def handle_result(err, user):
            if err:
                if isinstance(err, InvalidCredentialsException):
                    msg = QMessageBox(self)
                    msg.setText(
                        'Incorrect email or password. Please try ' +
                        'again or %s.' % get_link(RESET_PASSWORD_URL,
                                                  'reset your password'))
                    msg.show()
                else:
                    msg = QMessageBox(self)
                    msg.setText('Something happened during login. ' +
                                'Make sure you have internet connection and ' +
                                'try again. \n\nError: %s (%s)' %
                                (str(err), err.__class__.__name__))
                    msg.show()
            else:
                event.emit(event.EVENT['USER:LOGIN'], user)

            QApplication.restoreOverrideCursor()

        api.login(email, password, state.read('device/id'), handle_result)

    def handle_signup_click(self, email, password):
        QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)

        def handle_result(err, user):
            if err:
                if isinstance(err, AccountExistsException):
                    msg = QMessageBox(self)
                    msg.setText('The email address is already in use. Try ' +
                                'logging in instead. You can reset your ' +
                                'password %s if you don\'t remember it.' % (
                                    get_link(RESET_PASSWORD_URL, 'here')
                                ))
                    msg.show()
                elif isinstance(err, ValidationException):
                    msg = QMessageBox(self)
                    msg.setText(str(err))
                    msg.show()
                else:
                    msg = QMessageBox(self)
                    msg.setText('Something happened during sign up. ' +
                                'Make sure you have internet connection and ' +
                                'try again.\n\nError: %s (%s)' %
                                (str(err), err.__class__.__name__))
                    msg.show()
            else:
                event.emit(event.EVENT['USER:SIGNUP'], user)

            QApplication.restoreOverrideCursor()

        api.signup(email, password, state.read('device/id'), handle_result)
