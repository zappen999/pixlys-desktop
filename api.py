# The purpose of this module is to handle all communication with the API server

from config import HOST, APP_VERSION
from runtime import PLATFORM_SLUG
from exception import (InvalidCredentialsException, AccountExistsException,
                       ServerHTTPException, ServerConnectionException,
                       ValidationException)
import json
import platform

from PyQt5.QtNetwork import (QNetworkAccessManager, QNetworkRequest,
                             QHttpMultiPart, QHttpPart)
from PyQt5.QtCore import (QByteArray, QUrl, QVariant, QIODevice, QFile)

API_URL = HOST + '/api'

PLATFORM_DEBUG = '%s %s %s' % (
        platform.system(), platform.release(), platform.version())

manager = QNetworkAccessManager()


# TODO: Timeout functionality by starting timer and using abort()
def get_api_request(url, device_id):
    url = QUrl(url)
    req = QNetworkRequest(url)

    set_raw_header(req, 'X-Device-ID', device_id)
    set_raw_header(req, 'X-Platform-Debug', PLATFORM_DEBUG)
    set_raw_header(req, 'X-Platform-Slug', PLATFORM_SLUG)
    set_raw_header(req, 'X-Platform-Version', APP_VERSION)

    return req


def set_raw_header(req, key, val):
    header_key = QByteArray()
    header_key.append(key)
    header_val = QByteArray()
    header_val.append(val)
    req.setRawHeader(header_key, header_val)
    return req


def parse_json_reply(reply):
    json_data = reply.readAll().data().decode('utf8')
    return json.loads(json_data)


# TODO: This is fragile... Client crashes when server responds with plaintext
# for example.
def login(email, password, device_id, callback):
    req = get_api_request(API_URL + '/auth/login', device_id)

    json_data = QByteArray()
    json_data.append(json.dumps({'email': email, 'password': password}))

    req.setHeader(QNetworkRequest.ContentTypeHeader,
                  QVariant('application/json'))
    req.setHeader(QNetworkRequest.ContentLengthHeader,
                  QByteArray.number(json_data.size()))
    reply = manager.post(req, json_data)

    def handle_reply():
        http_code = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)

        if not http_code:
            callback(ServerConnectionException(reply.errorString()), None)
        elif http_code == 200:
            callback(None, parse_json_reply(reply))
        elif http_code == 401:
            callback(InvalidCredentialsException(), None)
        else:
            callback(ServerHTTPException(), None)

    reply.finished.connect(handle_reply)


# By providing the device ID, we can connect the previous uploads to the
# account.
def signup(email, password, device_id, callback):
    req = get_api_request(API_URL + '/auth/signup', device_id)

    json_data = QByteArray()
    json_data.append(json.dumps({'email': email, 'password': password}))

    req.setHeader(QNetworkRequest.ContentTypeHeader,
                  QVariant('application/json'))
    req.setHeader(QNetworkRequest.ContentLengthHeader,
                  QByteArray.number(json_data.size()))
    reply = manager.post(req, json_data)

    def handle_reply():
        http_code = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)

        if not http_code:
            callback(ServerConnectionException(reply.errorString()), None)
        elif http_code == 200:
            callback(None, parse_json_reply(reply))
        elif http_code == 409:
            callback(AccountExistsException(), None)
        elif http_code == 400:
            data = parse_json_reply(reply)
            callback(ValidationException(data['error']), None)
        else:
            callback(ServerHTTPException(), None)

    reply.finished.connect(handle_reply)


def upload(filepath, device_id, callback, on_progress=None):
    req = get_api_request(API_URL + '/uploads', device_id)

    form = QHttpMultiPart()
    form.setContentType(QHttpMultiPart.FormDataType)

    file_part = QHttpPart()
    # TODO: We should get the mime from the actual file we are sending
    file_part.setHeader(QNetworkRequest.ContentDispositionHeader,
                        QVariant('form-data; name="file"; filename="i.png"'))
    file_part.setHeader(QNetworkRequest.ContentTypeHeader,
                        QVariant('image/png'))
    fh = QFile(filepath)
    fh.open(QIODevice.ReadOnly)
    file_part.setBodyDevice(fh)
    fh.setParent(form)  # Will make the file get collected when done
    form.append(file_part)

    reply = manager.post(req, form)
    form.setParent(reply)  # Will make the form get GC:ed when done

    def handle_reply():
        fh.close()  # Close the file
        http_code = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)

        if not http_code:
            callback(ServerConnectionException(reply.errorString()), None)
        elif http_code == 200:
            callback(None, parse_json_reply(reply))
        else:
            callback(ServerHTTPException(), None)

    reply.finished.connect(handle_reply)

    # TODO: This is not working great...
    if on_progress:
        reply.uploadProgress.connect(
                lambda sent, tot: on_progress(round(sent/max(tot, 1) * 100)))
