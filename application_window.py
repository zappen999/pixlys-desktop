from PyQt5 import QtCore
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QApplication, QWidget, QPushButton,
                             QTabWidget, QMenu, QVBoxLayout,
                             QSystemTrayIcon, QMessageBox, QLayout)
import webbrowser
import sys

# from exception import ServerConnectionException
from config import APP_NAME, UPDATE_URL, ACCOUNT_URL
from runtime import PLATFORM_SLUG
from account_tab import AccountTab
from settings_tab import SettingsTab
from style import get_link
from data import get_path
import state
import screenshot
import api


class ApplicationWindow(QWidget):
    def __init__(self, app, instant_capture=False):
        super().__init__()

        self.app = app
        self.instant_capture = instant_capture

        # Widgets
        self.layout = None
        self.tabs = None
        self.tab1 = None
        self.tab2 = None
        self.close_btn = None

        # Context menu/tray
        self.ctx_menu = None
        self.app_icon = None
        self.upload_icon = None
        self.tray_icon = None

        self.init_ui()

        if self.instant_capture:
            print('Taking instant capture')
            self.take_screenshot()

    def take_screenshot(self):
        filepath = screenshot.get_temp_filepath()
        result = screenshot.screenshot(filepath)

        if not result:
            print('Screenshot was aborted by the user')
            if self.instant_capture:
                self.quit()

            return

        device_id = state.read('device/id')
        self.set_upload_tray_icon()

        def handle_result(err, upload):
            if err:
                self.handle_upload_error(err, filepath)
            else:
                self.handle_upload_success(upload, filepath)

            self.unset_upload_tray_icon()

            # Nothing more to do here if it's an instant capture
            if self.instant_capture:
                self.quit()

        api.upload(filepath, device_id, handle_result)

    def handle_upload_error(self, err, filepath):
        # TODO: Different messages for different causes?
        # if isinstance(err, ServerConnectionException):
        msg = QMessageBox(self)
        msg.setText('There was an error uploading the screenshot. Check ' +
                    'your network connection and try again. You can ' +
                    'access your screenshot on your computer ' +
                    get_link('file:///' + filepath, 'here'))
        msg.exec()

    def handle_upload_success(self, upload, filepath):
        if state.read('settings/open_in_browser'):
            webbrowser.open(upload['url'])

        if state.read('settings/copy_to_clipboard'):
            self.copy_to_clipboard(upload['url'])

        print(upload)

        if 'message' in upload:
            self.handle_message(upload['message'])

        screenshot.remove_file(filepath)

    # TODO: Test this
    def handle_message(self, message):
        if message['type'] == 'update':
            return self.handle_update_message(message)
        else:
            return self.handle_generic_message(message)

    def handle_update_message(self, message):
        msg = QMessageBox(self)
        msg.setText(message['text'])
        msg.addButton(QPushButton('Update now'), QMessageBox.YesRole)
        msg.addButton(QPushButton('Keep annoy me'), QMessageBox.RejectRole)
        index = msg.exec()

        if index == 0:
            webbrowser.open(UPDATE_URL % PLATFORM_SLUG)

    def handle_generic_message(self, message):
        msg = QMessageBox(self)
        msg.setText(message['text'])
        msg.addButton(QPushButton('Ok'), QMessageBox.YesRole)
        msg.exec()

    def copy_to_clipboard(self, value):
        clipboard = QApplication.clipboard()
        clipboard.clear(mode=clipboard.Clipboard)
        clipboard.setText(value, mode=clipboard.Clipboard)

    def handle_tray_icon_activation(self, reason):
        if reason == QSystemTrayIcon.ActivationReason.DoubleClick:
            self.take_screenshot()

    def set_upload_tray_icon(self):
        self.tray_icon.setIcon(self.upload_icon)

    def unset_upload_tray_icon(self):
        self.tray_icon.setIcon(self.app_icon)

    def open_account(self):
        webbrowser.open(ACCOUNT_URL)

    def hide_window(self):
        self.hide()

    def show_window(self):
        self.show()

        # Make the window take focus
        self.setWindowState(self.windowState() & ~QtCore.Qt.WindowMinimized |
                            QtCore.Qt.WindowActive)
        self.activateWindow()
        self.raise_()

    # Catch all events, in order to detect minimize event
    def event(self, event):
        if event.type() == QtCore.QEvent.WindowStateChange \
                and self.isMinimized():
            self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.Tool)
            return True
        else:
            # Let the event pass through
            return super(ApplicationWindow, self).event(event)

    def closeEvent(self, event):
        event.ignore()
        self.hide_window()

    def on_close(self):
        self.hide_window()

    def quit(self):
        print('Exiting from application window')
        QApplication.instance().quit()
        sys.exit(0)

    def init_ui(self):
        self.setWindowTitle(APP_NAME)
        # TODO: Start application without showing it in task bar (macOS)?
        # Seems to work fine on Gnome (Ubuntu 18.04)
        self.setWindowFlags(QtCore.Qt.WindowMinimizeButtonHint)

        self.layout = QVBoxLayout(self)

        # Make the layout recalc the height when children change size
        self.layout.setSizeConstraint(QLayout.SetFixedSize)

        # Tray icon & menu
        self.ctx_menu = QMenu()
        self.ctx_menu.addAction('Take Screenshot', self.take_screenshot)
        self.ctx_menu.addAction('My History', self.open_account)
        self.ctx_menu.addAction('Settings', self.show_window)
        self.ctx_menu.addAction('Exit', self.quit)
        # This seems to be the only way to support Unity?
        # self.ctx_menu.aboutToShow.connect(self.handle_open_event)

        self.app_icon = QIcon(get_path('icons/256x256.png'))
        self.upload_icon = QIcon(get_path('icons/256x256_ul.png'))
        self.tray_icon = QSystemTrayIcon(self.app_icon, parent=self)
        self.tray_icon.setContextMenu(self.ctx_menu)
        self.tray_icon.activated.connect(self.handle_tray_icon_activation)

        if not self.instant_capture:
            self.tray_icon.show()

        # Main layout
        self.tabs = QTabWidget()

        # Force a width for the entire application
        self.tabs.setMinimumSize(300, 0)
        self.tabs.setMaximumSize(300, 1000)

        self.tab1 = AccountTab()
        self.tab2 = SettingsTab()

        self.tabs.addTab(self.tab1, "Account")
        self.tabs.addTab(self.tab2, "Settings")

        self.close_btn = QPushButton('Close', self)
        self.close_btn.clicked.connect(lambda: self.hide_window())

        self.layout.addWidget(self.tabs)
        self.layout.addWidget(self.close_btn, 0, QtCore.Qt.AlignRight)

        self.setLayout(self.layout)
