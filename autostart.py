import sys
import os


def try_install():
    try:
        if sys.platform == 'win32':
            return  # Already handled by InnoSetup
        elif sys.platform == 'linux':
            return  # TODO: Soon
        elif sys.platform == 'darwin':
            install_darwin_autostart()
        else:
            pass  # Don't know how to do autostart on this system
    except Exception as ex:
        print('Could not install autostart', ex)
        return False


def install_darwin_autostart():
    launch_agent_plist_name = 'com.pixlys.autostart.plist'
    launch_agent_plist_path = os.path.join(os.path.expanduser('~'),
                                           'Library/LaunchAgents/',
                                           launch_agent_plist_name)
    if os.path.isfile(launch_agent_plist_path):
        return  # Already installed

    plist_content = '''<?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
        <dict>
            <key>Label</key>
            <string>%s</string>
            <key>Program</key>
            <string>/Applications/Pixlys.app/Contents/MacOS/pixlys</string>
            <key>RunAtLoad</key>
            <true/>
        </dict>
    </plist>''' % launch_agent_plist_name

    with open(launch_agent_plist_path, 'w+') as fh:
        print(plist_content, file=fh)
