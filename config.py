APP_NAME = 'Pixlys'
APP_VERSION = '3'
PRODUCTION = True

DOMAIN = 'pixlys.com' if PRODUCTION else 'localhost:8000'
PROTOCOL = 'https' if PRODUCTION else 'http'
HOST = PROTOCOL + '://' + DOMAIN

RESET_PASSWORD_URL = HOST + '/user/password-reset-request'
ACCOUNT_URL = HOST + '/user'
PRIVACY_URL = HOST + '/privacy'
TOS_URL = HOST + '/terms-of-service'
UPDATE_URL = HOST + '/download/%s?update=1'
