import sys
import os


# Resolves the path to files added to the application bundle
def get_path(path):
    if not hasattr(sys, '_MEIPASS'):  # Not running through pyinstaller
        return path

    return os.path.join(sys._MEIPASS, path)
