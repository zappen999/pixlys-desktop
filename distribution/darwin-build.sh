#!/bin/bash
############### BUILD VARIABLES ###############
export PACKAGE="Pixlys"
export VERSION="0.1.2"
export CERT_NAME="Developer ID Application: Johan Kanefur (9CFX5KSX72)"
###############################################
set -e

rm -rf dist/* build/* pixlys.spec node_modules/

pyinstaller \
    --add-data="icons/:icons/" \
    --noupx \
    --windowed \
    --icon="icons/256x256.icns" \
    pixlys.py

mkdir -p dist/installers

mv dist/pixlys.app dist/$PACKAGE.app

# Sign the app bundle
./distribution/darwin-sign.sh

# Create installer
# TODO: There is a bash version of appdmg, which means that we don't need
# to mix in node.js as a dependency.
# Using python2.7 since node-gyp requires it to build
npm install --python=python2.7 appdmg plist

node distribution/darwin-appdmg-installer.js

# Cleanup
rm -rf node_modules/ package-lock.json
