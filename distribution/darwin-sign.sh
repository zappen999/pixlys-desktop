#!/bin/bash
# This script is used to sign the .app file and all it's contents

# Sign app
codesign --force --verify --timestamp --sign "${CERT_NAME}" --deep ./dist/Pixlys.app

echo "Verifying signing of the .app file..."
codesign -vvv -d ./dist/Pixlys.app
