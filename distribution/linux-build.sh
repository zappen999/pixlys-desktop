#!/bin/bash
PACKAGE="pixlys"
VERSION="0.1.0"

rm -rf dist/* build/*

pyinstaller \
    --add-data icons/:icons/ \
    --noupx \
    --windowed \
    pixlys.py

mv dist/pixlys dist/$PACKAGE-$VERSION
cd dist/$PACKAGE-$VERSION
