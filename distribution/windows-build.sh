#!/bin/bash
#
# Build script for Windows. Tested on W7 using git bash. Please read the
# README file for more information about setup before this script should
# be run. Note that problems may occur when building on W10:
# - https://github.com/cowo78/pyinstaller/commit/6198e640670eb7ef0d1e2a7b0bac81028f620ce5#diff-665ffb7d19093ce3b903440e23eb6e98R460
# - https://github.com/pyinstaller/pyinstaller/issues/1566
# 
# To get a smaller build, install UPX at the directory specified in the
# command below.
#
# NOTE: There may be problems with Windows Defender during build. Don't
# know whats causing it yet.
#
# NOTE 2: There seems to be a problem when using UPX to compress the final
# binary (I get a DDL load error on startup).
#
# TODO: Include the screenshot utility binary for Windows using --add-data
set -e

pip3 install pyinstaller

# TODO: Should we preserve something between builds?
rm -rf dist/* build/* pixlys.spec

pyinstaller \
	--add-data="icons\;icons" \
	--noupx \
	--windowed \
	--icon="icons\256x256.ico" \
	pixlys.py

mkdir -p dist/installers

# TODO: InnoSetup automation can be placed here
# Copy the built screenshot executable
cp \
	../pixlys-win-screenshot/pixlys-win-screenshot/Release/pixlys-win-screenshot.exe \
	dist/pixlys/pixlys-win-screenshot.exe
