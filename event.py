# This module acts like a singleton for application-wide events
import traceback

subscribers = {}
EVENT = {
    'USER:LOGIN': 'USER:LOGIN',
    'USER:SIGNUP': 'USER:SIGNUP',
    'USER:UPDATED': 'USER:UPDATED',
}


def on(event, callback):
    if type(event) is list:
        for e in event:
            on(e, callback)

        return

    if event not in subscribers:
        subscribers[event] = []

    subscribers[event].append(callback)


def emit(event, data):
    if event not in subscribers:
        return  # Nothing to do here

    for subscriber in subscribers[event]:
        try:
            subscriber(data)
        except Exception as e:
            print('Error in event handler: %s' % traceback.format_exc())
