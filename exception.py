class InvalidCredentialsException(Exception):
    pass


class AccountExistsException(Exception):
    pass


class ValidationException(Exception):
    pass


class ServerConnectionException(Exception):
    pass


class ServerHTTPException(Exception):
    pass


class ScreenshotException(Exception):
    pass
