from PyQt5 import QtCore
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import (QWidget, QPushButton, QVBoxLayout, QHBoxLayout,
                             QLabel)

import style
from config import APP_NAME
from data import get_path


class FirstUsageWindow(QWidget):
    def __init__(self, on_connect_account=False):
        super().__init__()

        self.layout = None

        self.on_connect_account = on_connect_account

        self.init_ui()

    def handle_ignore(self):
        self.hide()

    def handle_connect_account(self):
        self.hide()

        if self.on_connect_account:
            self.on_connect_account()

    def init_ui(self):
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setMinimumSize(0, 600)
        self.layout = QVBoxLayout(self)
        self.layout.setContentsMargins(32, 32, 32, 32)

        title_lbl = QLabel()
        title_lbl.setText('Get started')
        title_lbl.setStyleSheet(style.title())
        self.layout.addWidget(title_lbl, 0, QtCore.Qt.AlignHCenter)

        img_lbl = QLabel()
        img_lbl.setPixmap(QPixmap(get_path('icons/256x256.png')))
        img_lbl.setStyleSheet(style.splash_img())
        self.layout.addWidget(img_lbl, 0, QtCore.Qt.AlignHCenter)

        description_lbl = QLabel()

        description_text = (
            'To take a screenshot, click on the %s logo in the corner of '
            'your screen and click on "Take Screenshot". '
            'After that you\'ll be able to access and '
            'share your screenshot by pasting the URL.\n\n'

            'Remember, if you want to keep your screenshot history, '
            'you\'ll have to connect to your account in settings.'
        ) % APP_NAME

        description_lbl.setText(description_text)
        description_lbl.setWordWrap(True)
        description_lbl.setStyleSheet(style.splash_text())
        self.layout.addWidget(description_lbl, 0, QtCore.Qt.AlignHCenter)

        btn_group = QHBoxLayout()
        btn_group.setAlignment(QtCore.Qt.AlignCenter)

        ignore_btn = QPushButton('Continue without account')
        ignore_btn.clicked.connect(self.handle_ignore)
        btn_group.addWidget(ignore_btn)

        connect_btn = QPushButton('Connect account →')
        connect_btn.setDefault(True)
        connect_btn.clicked.connect(self.handle_connect_account)
        btn_group.addWidget(connect_btn)

        self.layout.addLayout(btn_group)
        self.show()
