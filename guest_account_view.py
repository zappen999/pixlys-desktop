from PyQt5 import QtCore
from PyQt5.QtWidgets import (QWidget, QPushButton, QHBoxLayout, QVBoxLayout,
                             QLabel)
import style


class GuestAccountView(QWidget):
    def __init__(self):
        super().__init__()

        self.layout = None
        self.login_link_btn = None
        self.signup_link_btn = None

        self.init_ui()

    def on_login(self, callback):
        self.login_link_btn.clicked.connect(callback)

    def on_signup(self, callback):
        self.signup_link_btn.clicked.connect(callback)

    def init_ui(self):
        self.layout = QVBoxLayout(self)
        btn_group = QHBoxLayout()

        account_info_lbl = QLabel(
                'Sign in or create an account to keep your ' +
                'screenshot history.')
        account_info_lbl.setStyleSheet(style.label('WARNING'))
        account_info_lbl.setWordWrap(True)
        self.layout.addWidget(account_info_lbl, 0, QtCore.Qt.AlignCenter)

        self.login_link_btn = QPushButton('Sign in')
        btn_group.addWidget(self.login_link_btn, 0)

        self.signup_link_btn = QPushButton('Create account')
        # self.signup_link_btn.clicked.connect(lambda: self.hide())
        btn_group.addWidget(self.signup_link_btn, 0)

        self.layout.addLayout(btn_group)
        self.layout.addStretch()
