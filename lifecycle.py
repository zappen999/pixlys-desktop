import socket

HOPEFULLY_UNUSED_PORT = 7890
sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)


# We are using sockets to test if another instance of the program is running
# currently.
def is_other_instance_running():
    global sock

    try:
        sock.bind(('localhost', HOPEFULLY_UNUSED_PORT))
    except OSError as e:
        return True

    return False
