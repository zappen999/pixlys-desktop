from PyQt5.QtWidgets import (QWidget, QVBoxLayout, QHBoxLayout, QLabel,
                             QPushButton)
import webbrowser

from config import ACCOUNT_URL
import style
import state
import event


class MemberView(QWidget):
    def __init__(self):
        super().__init__()

        self.layout = None

        self.logout_link_btn = None
        self.member_lbl = None

        # Bind events
        event.on(event.EVENT['USER:UPDATED'],
                 lambda _: self.update_account_lbl())

        self.init_ui()

    def on_logout(self, callback):
        self.logout_link_btn.clicked.connect(callback)

    def update_account_lbl(self):
        self.member_lbl.setText(self.get_account_lbl_text())

    def get_account_lbl_text(self):
        email = state.read('user/email')
        return 'Connected as %s' % email

    def init_ui(self):
        self.layout = QVBoxLayout(self)

        self.member_lbl = QLabel(self.get_account_lbl_text())
        self.member_lbl.setStyleSheet(style.label('INFO'))
        self.member_lbl.setWordWrap(True)
        self.member_lbl.setOpenExternalLinks(True)
        self.layout.addWidget(self.member_lbl)

        btn_group = QHBoxLayout()
        account_link_btn = QPushButton('My account')
        account_link_btn.clicked.connect(lambda: webbrowser.open(ACCOUNT_URL))
        btn_group.addWidget(account_link_btn, 0)

        self.logout_link_btn = QPushButton('Logout')
        btn_group.addWidget(self.logout_link_btn, 0)

        self.layout.addStretch()
        self.layout.addLayout(btn_group, 0)
