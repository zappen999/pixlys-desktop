#!/usr/bin/env python3
# The purpose of this module is to act as entrypoint for a cross-platform GUI
# for screenshot utility Pixlys.

import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize, QCoreApplication

from application_window import ApplicationWindow
from first_usage_window import FirstUsageWindow
from config import APP_NAME, APP_VERSION, DOMAIN
from data import get_path
import style
import state
import lifecycle
import autostart

if __name__ == '__main__':
    try:
        is_other_instance_running = lifecycle.is_other_instance_running()
    except Exception as e:
        print('Could not determine if other instance is running', e)
        # Rather have the user open 2 instances of the application than
        # preventing them from starting the first one.
        is_other_instance_running = False

    QCoreApplication.setOrganizationName(APP_NAME)
    QCoreApplication.setOrganizationDomain(DOMAIN)
    QCoreApplication.setApplicationName(APP_NAME)
    QCoreApplication.setApplicationVersion(APP_VERSION)

    autostart.try_install()

    app = QApplication(sys.argv)
    app.setStyleSheet(style.app())

    app_icon = QIcon()
    app_icon.addFile(get_path('icons/16x16.png'), QSize(16, 16))
    app_icon.addFile(get_path('icons/24x24.png'), QSize(24, 24))
    app_icon.addFile(get_path('icons/32x32.png'), QSize(32, 32))
    app_icon.addFile(get_path('icons/48x48.png'), QSize(48, 48))
    app_icon.addFile(get_path('icons/256x256.png'), QSize(256, 256))
    app.setWindowIcon(app_icon)
    app.setQuitOnLastWindowClosed(False)

    state.set_initial_state()

    setting_window = ApplicationWindow(
            app, instant_capture=is_other_instance_running)
    setting_window.hide()

    if not state.exists('setup/usage_guide_shown'):
        first_usage_window = FirstUsageWindow(
                on_connect_account=setting_window.show_window)
        first_usage_window.show()
        state.write('setup/usage_guide_shown', 1)

    sys.exit(app.exec_())
