import sys

# Maps platform to slug used in API
pmap = {
    'linux': 'linux',
    'win32': 'windows',
    'darwin': 'macos'
}


PLATFORM_SLUG = pmap[sys.platform] if sys.platform in pmap else 'unknown'
