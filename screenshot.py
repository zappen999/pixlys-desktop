# The purpose of this module is to provide a cross-platform way of spawning
# a screenshot process.

import sys
import tempfile
import uuid
import os

from config import APP_NAME
import screenshot_linux
import screenshot_darwin
import screenshot_win32


# TODO: Use QProcess to launch non-blocking and use signals
def screenshot(destination, do_selection=True):
    module = None

    if sys.platform == 'linux':
        module = screenshot_linux
    elif sys.platform == 'darwin':
        module = screenshot_darwin
    elif sys.platform == 'win32':
        module = screenshot_win32
        pass

    if not module:
        raise Exception('Unsupported platform')

    module.screenshot(destination, do_selection=do_selection)

    if do_selection and not os.path.isfile(destination):
        # The user aborted the screenshot
        return False

    return True


def get_temp_filepath():
    tempdir = tempfile.gettempdir()
    filename = APP_NAME.lower() + '_' + str(uuid.uuid4()) + '.png'
    return os.path.join(tempdir, filename)


def remove_file(filepath):
    os.remove(filepath)
