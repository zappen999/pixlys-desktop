import os
import shutil
import subprocess

from exception import ScreenshotException


def screenshot(destination, do_selection=True):
    impl = get_screenshot_method()
    return impl(destination, do_selection)


# Implementations

# screencapture -s $file
# -m only capture main
# -D 1 -D 2 specifies monitor
def impl_screencapture(destination, do_selection):
    args = ['screencapture']

    if do_selection:
        args.append('-s')
    else:
        args.append('-m')

    args.append(destination)
    p = subprocess.Popen(args)
    p.communicate()  # Waits for process to terminate

    if p.returncode != 0:
        raise ScreenshotException(
                'Screencapture exit with non-zero code: %s' % p.returncode)


def get_screenshot_method():
    if shutil.which('screencapture', mode=os.X_OK) is not None:
        return impl_screencapture

    raise ScreenshotException('Found no implementation for darwin')
