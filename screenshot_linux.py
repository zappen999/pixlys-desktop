# Test different screenshot tools
import os
import shutil
import subprocess
import time

from exception import ScreenshotException


def screenshot(destination, do_selection=True):
    time.sleep(0.05)  # Let the tray menu close before printing

    # Find a way to make it happen
    impl = get_screenshot_method()
    return impl(destination, do_selection)


# Implementations
def impl_scrot(destination, do_selection):
    # Quality when using png in scrot means compression level
    args = ['scrot', '--quality', '10', '--silent', destination]

    p = subprocess.Popen(args)
    p.communicate()  # Waits for process to terminate

    if p.returncode != 0:
        raise ScreenshotException(
                'Scrot exit with non-zero code: %s' % p.returncode)


def impl_gnome_screenshot(destination, do_selection):
    args = ['gnome-screenshot', '--file', destination]

    if do_selection:
        args.append('--area')

    p = subprocess.Popen(args)
    p.communicate()  # Waits for process to terminate

    if p.returncode != 0:
        raise ScreenshotException(
                'Gnome-screenshot exit with non-zero code: %s' % p.returncode)


# TODO: I want the same experience across Linux distributions, until then,
# we just test for existing screenshot executables and call it a day.
# Inspiration:
# https://gitlab.gnome.org/GNOME/gnome-screenshot/blob/master/src/screenshot-area-selection.c
def get_screenshot_method():
    if shutil.which('scrot', mode=os.X_OK) is not None:
        return impl_scrot

    if shutil.which('gnome-screenshot', mode=os.X_OK) is not None:
        return impl_gnome_screenshot
