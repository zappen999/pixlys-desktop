import subprocess
from data import get_path

from exception import ScreenshotException


def screenshot(destination, do_selection=True):
    args = [get_path('pixlys-win-screenshot.exe')]

    if not do_selection:
        args.append('--fullscreen')

    args.append(destination)

    p = subprocess.Popen(args)
    p.communicate()  # Waits for process to terminate

    if p.returncode != 0:
        raise ScreenshotException(
                'Pixlys screenshot exit with non-zero code: %s' % p.returncode)
