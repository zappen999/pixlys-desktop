from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QCheckBox

import state


class SettingsTab(QWidget):
    def __init__(self):
        super().__init__()

        # Widgets
        self.layout = None
        self.open_in_browser_chk = None
        self.copy_to_clipboard_chk = None

        self.init_ui()

    def handle_settings_update(self):
        state.write('settings/open_in_browser',
                    int(self.open_in_browser_chk.isChecked()))
        state.write('settings/copy_to_clipboard',
                    int(self.copy_to_clipboard_chk.isChecked()))

    def init_ui(self):
        self.layout = QVBoxLayout(self)

        # Setting components
        self.open_in_browser_chk = QCheckBox(
                "Show image after screenshot", self)
        self.open_in_browser_chk.setChecked(
                bool(int(state.read('settings/open_in_browser', 1))))
        self.open_in_browser_chk.toggled.connect(self.handle_settings_update)
        self.open_in_browser_chk.setToolTip(
                'Opens the screenshot in your browser once it\'s uploaded')

        self.copy_to_clipboard_chk = QCheckBox(
                "Copy URL to image after screenshot", self)
        self.copy_to_clipboard_chk.setChecked(
                bool(int(state.read('settings/copy_to_clipboard', 1)))
        )
        self.copy_to_clipboard_chk.toggled.connect(
                self.handle_settings_update)
        self.copy_to_clipboard_chk.setToolTip(
                'Places the screenshot URL in your clipboard')

        self.layout.addWidget(self.open_in_browser_chk, 0,
                              QtCore.Qt.AlignTop)
        self.layout.addWidget(self.copy_to_clipboard_chk, 0,
                              QtCore.Qt.AlignTop)
        self.layout.addStretch()
