from PyQt5 import QtCore
import uuid

# In-memory storage
state = {}

# Permanent storage instance
settings = None


# Get's the permanent setting  store. We cannot declare it directly since
# application settings needs to be set first.
def get_settings():
    global settings

    if settings:  # Memoization
        return settings

    settings = QtCore.QSettings()
    print('Loading from config file: ', settings.fileName())
    return settings


def read(key, default_value=None, permanent=True):
    settings = get_settings()
    return settings.value(key, default_value)


def write(key, value, permanent=True):
    if permanent:
        settings = get_settings()
        settings.setValue(key, value)
        settings.sync()
    else:
        state[key] = value


def exists(key, permanent=True):
    if permanent:
        settings = get_settings()
        return settings.contains(key)

    return key in state


def reset():
    settings = get_settings()
    settings.clear()
    set_initial_state()


def set_initial_state():
    if not exists('device/id'):
        write('device/id', str(uuid.uuid4()))

    if not exists('settings/open_in_browser'):
        write('settings/open_in_browser', '1')

    if not exists('settings/copy_to_clipboard'):
        write('settings/copy_to_clipboard', '1')
