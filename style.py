import color


def app():
    return '''
        QWidget {
            font-size: 13px;
        }

        QLineEdit {
            padding: 4px;
        }

        /* QCheckBox only used for signup form atm */
        QLabel, QCheckBox {
            margin-top: 4px;
            margin-bottom: 2px;
        }
    '''


def label(severity='INFO'):
    label_color_scheme = None

    if severity == 'INFO':
        label_color_scheme = color.INFO
    elif severity == 'WARNING':
        label_color_scheme = color.WARNING

    return '''QLabel {
        color: %s;
        border: 1px solid %s;
        padding: 8px;
        border-radius: 2px;
    }''' % (label_color_scheme, label_color_scheme)


def title():
    return '''QLabel {
        font-size: 32px;
    }'''


def splash_img():
    return '''QLabel {
        margin: 32px;
    }'''


def splash_text():
    return '''QLabel {
        max-width: 500px;
        margin-bottom: 16px;
    }'''


def get_link(url, text):
    return '<a href="%s" style="color: %s">%s</a>' % (url, color.LINK, text)
